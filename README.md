# Czekolada Kane'a

Domowa, kryzysowa, blok czekoladowy - jak zwał tak zwał - ważne, że swojskie i dobre :-)

Obecny przepis już dość mocno odbiega od oryginału znalezionego w sieci - został uwspółcześniony/ulepszony, tj. zawiera masło zamiast margaryny, ma 2x mniej cukru, więcej kakao etc. Sprawdź historię jeśli jesteś ciekawy(a).

## Składniki ##
* 200g masła (czyli kostka lub trochę więcej)
* cukier waniliowy
* 2/3 szklanki cukru (lub mniej, czytaj niżej)
* 2/5 szklanki mleka
* kilka łyżek gorzkiego kakao
* 30..50 dag mleka w proszku
* szczypta soli
* papier do pieczenia do wyłożenia blaszki/foremki
* dodatki:
	* pokruszone herbatniki
	* crunchy w drobnych kawałkach
	* maliny lub/i borówki i/lub żurawina suszona
	* dmuchany ryż
	* płatki owsiane
	* płatki Lion
	* etc.

## Sposób przygotowania ##
Cukier i cukier waniliowy wymieszać z mlekiem (najlepiej lekko ciepłym) lub wody, dodać masło oraz szczyptę soli i stale mieszając, podgrzewać na małym ogniu do chwili, gdy powstanie jednolita masa.

Następnie powoli, ciągle mieszając dodać kakao.
 
Po wymieszaniu kakao powoli dodawaj mleko w proszku (i mieszaj) aż powstała masa będzie miała kolor czekolady mlecznej (i będzie bardziej gęsta niż płynna).

Wyrobić na jednolity, elastyczny krem. Wrzucić dodatki - najlepiej paczkę/dwie dobrych (ciężkich) herbatników i crunchy; wymieszać. Mieć na uwadze, że duża ilość słodkich dodatków powinna zmniejszyć udział cukru z listy składników.

Przełożyć do blaszki/foremki. Jak lekko ostygnie wstawić do lodówki aż dobrze zastygnie. Gotowe :-) !
